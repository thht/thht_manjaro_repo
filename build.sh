#!/usr/bin/env bash
cd
mkdir pkg

mkdir .gnupg
touch .gnupg/gpg.conf
echo "keyserver-options auto-key-retrieve" > .gnupg/gpg.conf
echo "keyserver hkps://keys.openpgp.org/" >> .gnupg/gpg.conf

for key in $FETCH_GPG_KEYS; do
  gpg --recv-key $key
done

rclone config create repo webdav url https://oc.th-ht.de/remote.php/dav/files/thht_manjaro_repo/ vendor nextcloud user thht_manjaro_repo pass $RCLONEPASS

export PKGDEST=/repo/assets

rclone copy repo:/thht_manjaro_repo /repo
rm /repo/assets/thht_manjaro.db.tar.gz.lck

ls /repo/

for package in $PACKAGES; do
  cd ~/pkg
  git clone https://aur.archlinux.org/$package || aws sns publish --topic-arn arn:aws:sns:eu-west-1:706029361197:BuildJobFailed --message "clone failed"
  cd $package

  depends=(); makedepends=(); checkdepends=()
  # shellcheck disable=1091
  . ./PKGBUILD
  deps=( "${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}" )
  pacman --deptest "${deps[@]}" | xargs yay -Sy --noconfirm || aws sns publish --topic-arn arn:aws:sns:eu-west-1:706029361197:BuildJobFailed --message "installing deps failed"

  pkg_files=$(makepkg --packagelist)
  do_build=false

  for cur_file in $pkg_files; do
    if [ ! -f $cur_file ] ; then
      do_build=true
    fi
  done

  # Do the actual building
  if [ "$do_build" = true ] ; then
    for cur_pkg in $pkg_files; do
      old_file_name="${cur_pkg%%-[0-9]*}-*.pkg.tar.zst"
      rm -rf $old_file_name
    done
    makepkg --skippgpcheck || aws sns publish --topic-arn arn:aws:sns:eu-west-1:706029361197:BuildJobFailed --message "makepkg failed"
  fi
done

rm -f $PKGDEST/thht_manjaro.*.tar.gz
rm -f $PKGDEST/thht_manjaro.db
rm -f $PKGDEST/thht_manjaro.files
repo-add $PKGDEST/thht_manjaro.db.tar.gz $PKGDEST/*.pkg.tar.zst
cd $PKGDEST

rclone sync -L /repo repo:/thht_manjaro_repo
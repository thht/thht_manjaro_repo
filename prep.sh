#!/usr/bin/env bash

pamac upgrade --no-confirm
pamac install --no-confirm sudo yay base-devel tree wget rclone aws-cli
useradd -m notroot
echo "notroot ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/notroot

mkdir /repo
chmod a+rwx /repo

sudo -u notroot --preserve-env=PACKAGES --preserve-env=RCLONEPASS --preserve-env=AWS_CONTAINER_CREDENTIALS_RELATIVE_URI --preserve-env=AWS_REGION --preserve-env=AWS_DEFAULT_REGION --preserve-env=FETCH_GPG_KEYS /scripts/build.sh
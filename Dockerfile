FROM manjarolinux/base

RUN pacman --noconfirm -Syu pamac
RUN pacman -Syuu --noconfirm

RUN mkdir /scripts

ADD build.sh /scripts/build.sh
ADD prep.sh /scripts/prep.sh

ENV PACKAGES='linux-lqx htcondor praat ausweisapp2'
ENV FETCH_GPG_KEYS='38DBBDC86092693E'

RUN chmod a+x /scripts/*.sh

CMD /scripts/prep.sh